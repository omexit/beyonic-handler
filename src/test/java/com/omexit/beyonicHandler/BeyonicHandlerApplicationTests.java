package com.omexit.beyonicHandler;

import com.omexit.beyonicHandler.util.DateUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BeyonicHandlerApplication.class)
public class BeyonicHandlerApplicationTests {

	@Test
	public void contextLoads() {
		System.err.println(DateUtil.parseDate("2016-09-22T14:10:24.394730Z", DateUtil.BEYONIC_DATE_FORMAT));
	}

}
