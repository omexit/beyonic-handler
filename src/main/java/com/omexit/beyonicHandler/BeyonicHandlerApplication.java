package com.omexit.beyonicHandler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class BeyonicHandlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeyonicHandlerApplication.class, args);
	}
}
