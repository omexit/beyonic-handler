package com.omexit.beyonicHandler.payment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Created by Antony on 2/16/2016.
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private final BeyonicProperties beyonicProperties;
    private final PaymentRepository paymentRepository;

    @Autowired
    public PaymentServiceImpl(BeyonicProperties beyonicProperties, PaymentRepository paymentRepository) {
        this.beyonicProperties = beyonicProperties;
        this.paymentRepository = paymentRepository;
    }


    @Override
    public void sendRequestToBeyonic(Payment payment) throws IOException {
        // JSONObject requestJsonObject = new JSONObject(strJsonRequest);
        // String requestPhoneNumber =
        // requestJsonObject.optString("phone_number");
        // Object requestAmount = requestJsonObject.get("amount");
        // String requestCurrency = requestJsonObject.optString("currency");
        // String requestDescription =
        // requestJsonObject.optString("description");
        //
        // Map<String, String> data = new HashMap<String, String>();
        // data.put("phonenumber", requestPhoneNumber);
        // data.put("amount", String.valueOf(requestAmount));
        // data.put("currency", requestCurrency);
        // data.put("description", requestDescription);
        // data.put("payment_type", "Money");
        // data.put("callback_url", beyonicProperties.getCallbackUrl());

//        Call<BeyonicPaymentResponse> paymentCall = beyonicHelper.sendNewPayment(payment.getPhoneNumber(),
//                payment.getCurrency(), String.valueOf(payment.getAmount()), payment.getDescription(),
//                beyonicProperties.getCallbackUrl(), "Money");
//        Response<BeyonicPaymentResponse> paymentCallResponse = null;
//        paymentCallResponse = paymentCall.execute();
//
//        BeyonicPaymentResponse paymentResponse = paymentCallResponse.body();
//
//        if (paymentResponse != null) {
//            payment.setAmount(Double.parseDouble(paymentResponse.getAmount()));
//            payment.setAuthor(paymentResponse.getAuthor());
//            payment.setCancelledBy(paymentResponse.getCancelledBy());
//            payment.setCancelledReason(paymentResponse.getCancelledReason());
//            payment.setCancelledTime(DateUtil.parseDate(paymentResponse.getCancelledTime(), "YYYY-MM-DDTHH:MM:SSZ"));
//            payment.setCreatedOnBeyonic(DateUtil.parseDate(paymentResponse.getCreated(), "YYYY-MM-DDTHH:MM:SSZ"));
//            payment.setCurrency(paymentResponse.getCurrency());
//            payment.setDescription(paymentResponse.getDescription());
//            payment.setLastError(paymentResponse.getLastError());
//            payment.setMetadata(new JSONObject(paymentResponse.getMetadata()).toString());
//            payment.setModifiedOnBeyonic(DateUtil.parseDate(paymentResponse.getModified(), "YYYY-MM-DDTHH:MM:SSZ"));
//            payment.setOrganization(paymentResponse.getOrganization());
//            payment.setPaymentId(paymentResponse.getId());
//            payment.setPaymentType(paymentResponse.getPaymentType());
//            payment.setPhoneNumber(paymentResponse.getPhoneNos().get(0));
//            payment.setRejectedBy(paymentResponse.getRejectedBy());
//            payment.setRejectedReason(paymentResponse.getRejectedReason());
//            payment.setRejectedTime(DateUtil.parseDate(paymentResponse.getRejectedTime(), "YYYY-MM-DDTHH:MM:SSZ"));
//            payment.setState(BeyonicPaymentStateType.fromString(paymentResponse.getState()));
//            payment.setUpdatedBy(paymentResponse.getUpdatedBy());
//            payment.setIsPaymentPostedToMifos(false);
//
//            savePayment(payment);
//        }

        // HttpRequest request =
        // HttpRequest.post(beyonicProperties.getEndPoint());
        // request.trustAllCerts();
        // request.trustAllHosts();
        // request.accept("application/json");
        // request.form(data);
        //
        // String response = request.body();
        // logger.info("Simfetch: " + response);

        // if(response!=null){
        // JSONObject responseJsonObject=new JSONObject(response);
        // Long id=responseJsonObject.getLong("id");
        // Long organization =responseJsonObject.getLong("organization");
        // double amount=responseJsonObject.getDouble("amount");
        // String currency =responseJsonObject.getString("currency");
        // String payment_type =responseJsonObject.getString("payment_type");
        // String metadata =responseJsonObject.getString("metadata");
        // String description =responseJsonObject.getString("description");
        // String phone_nos =responseJsonObject.getString("phone_nos");
        // String state =responseJsonObject.getString("state");
        // String lastError =responseJsonObject.getString("last_error");
        // String rejectedReason
        // =responseJsonObject.getString("rejected_reason");
        // String rejectedBy =responseJsonObject.getString("rejected_by");
        // String rejectedTime =responseJsonObject.getString("rejected_time");
        // String cancelledReason
        // =responseJsonObject.getString("cancelled_reason");
        // String cancelledBy =responseJsonObject.getString("cancelled_by");
        // String cancelledTime =responseJsonObject.getString("cancelled_time");
        // String created =responseJsonObject.getString("created");
        // String author =responseJsonObject.getString("author");
        // String modified =responseJsonObject.getString("modified");
        // String updated_by =responseJsonObject.getString("updated_by");
        // String start_date =responseJsonObject.getString("start_date");
        // }

    }

//    @Scheduled(fixedDelayString = "${processPayment.fixedDelay}")
    @Override
    public void processPayment() {
        // Find payments to process
        List<Payment> payments = paymentRepository.findByPaymentStatus(0);

        if (!payments.isEmpty()) {
            logger.info("Fetched: " + payments.size() + " payment records to process");
            for (Payment payment : payments) {
                try {
                    sendRequestToBeyonic(payment);

                    //update payment status so that this function pass over it like passover
                    payment.setPaymentStatus(1);

                    savePayment(payment);

                } catch (IOException e) {
                    logger.error(e.getMessage(), e);

                }
            }
        }
    }

    @Override
    public Payment savePayment(Payment payment) {
        return paymentRepository.save(payment);
    }

    @Override
    public Payment findByPaymentId(Long paymentId) {
        return paymentRepository.findByPaymentId(paymentId);
    }
}
