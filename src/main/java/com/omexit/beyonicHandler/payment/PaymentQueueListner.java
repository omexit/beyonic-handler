package com.omexit.beyonicHandler.payment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.omexit.beyonicHandler.jsonObjects.payment.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by aomeri on 9/19/16.
 */
@Component
public class PaymentQueueListner {

    private final BeyonicTask beyonicTask;

    private final ThreadPoolTaskExecutor taskExecutor;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public PaymentQueueListner(BeyonicTask beyonicTask, ThreadPoolTaskExecutor taskExecutor) {
        this.beyonicTask = beyonicTask;
        this.taskExecutor = taskExecutor;
    }

    /**
     * RabbitMQ requests listener.
     *
     * @param payload - JSON payment request
     */
    @RabbitListener(queues = "${beyonicHandler.channelName:beyonic}")
    public void process(@Payload String payload) {
        logger.debug("New payment request - {}", payload);

        beyonicTask.setPayload(payload);
        taskExecutor.execute(beyonicTask);
    }

}
