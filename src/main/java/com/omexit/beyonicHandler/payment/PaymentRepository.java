package com.omexit.beyonicHandler.payment;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

/**
 * Created by Antony on 2/17/2016.
 */
@Repository
@RestResource(path = "beyonic/v1/payments", rel = "payments")
public interface PaymentRepository extends JpaRepository<Payment, Long> {
	List<Payment> findAll();

	Payment findByPaymentId(Long paymentId);

	List<Payment> findByPaymentStatus(Integer paymentStatus);
}
