package com.omexit.beyonicHandler.payment;

import java.io.IOException;

/**
 * Created by Antony on 2/15/2016.
 */
public interface PaymentService {

    void sendRequestToBeyonic(Payment payment) throws IOException;

    void processPayment();

    Payment savePayment(Payment payment);

    Payment findByPaymentId(Long paymentId);
}
