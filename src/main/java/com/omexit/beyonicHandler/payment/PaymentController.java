package com.omexit.beyonicHandler.payment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.omexit.beyonicHandler.externalApi.beyonic.BeyonicPaymentStateType;
import com.omexit.beyonicHandler.jsonObjects.beyonicResponse.BeyonicPaymentResponse;
import com.omexit.beyonicHandler.util.BaseController;
import com.omexit.beyonicHandler.util.DateUtil;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Antony on 2/17/2016.
 */
@RestController
public class PaymentController extends BaseController {

    @Autowired
    PaymentService paymentService;

    /**
     * This endpoint receives payment requests then store in the queue for
     * processing
     *
     * @param request
     * @return
     * @throws JsonProcessingException
     */
//    @RequestMapping(value = PAYMENT_URL, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<PaymentResponse> createNewPayment(@RequestBody PaymentRequest request) throws JsonProcessingException {
//
//        Payment payment = new Payment();
//        payment.setPaymentBridgeRef(request.getPaymentBridgeRef());
//        payment.setPhoneNumber(request.getAccountNumber());
//        payment.setCurrency(request.getCurrency());
//        payment.setAmount(request.getAmount());
//        payment.setDescription(request.getDescription());
//        payment.setPaymentBridgeCallbackURL(request.getCallbackUrl());
//        payment.setIsPaymentPostedToMifos(false);
//        payment.setPaymentStatus(0);
//
//        payment = paymentService.savePayment(payment);
//
//
//        PaymentResponse response = new PaymentResponse();
//        response.setPaymentBridgeRef(payment.getPaymentBridgeRef());
//        response.setExternalId(String.valueOf(payment.getId()));
//        response.setStatus("1");
//        response.setPaymentBridgeRef(payment.getPaymentBridgeRef());
//        response.setMessage(String.format("Request ID: %s. Submited on - %s. PaymentRequest in progress", payment.getId(),
//                DateUtil.formatDate(payment.getDateCreated(), "dd-MM-yyyy")));
//
//        return new ResponseEntity<>(response, HttpStatus.CREATED);
//    }


    /**
     * Provides callback method for executed payment requests to beyonic.
     *
     * @param paymentResponse - Response object from beyonic payment system
     * @return
     */
    @RequestMapping(value = PAYMENT_CALLBACK_URL,
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> beyonicPaymentCallback(@RequestBody BeyonicPaymentResponse paymentResponse) {

        Payment payment = paymentService.findByPaymentId(paymentResponse.getId());
        if (payment == null) {
            payment = new Payment();
        }

        payment.setAmount(Double.parseDouble(paymentResponse.getAmount()));
        payment.setAuthor(paymentResponse.getAuthor());
        payment.setCancelledBy(paymentResponse.getCancelledBy());
        payment.setCancelledReason(paymentResponse.getCancelledReason());
        payment.setCancelledTime(DateUtil.parseDate(paymentResponse.getCancelledTime(), "YYYY-MM-DDTHH:MM:SSZ"));
        payment.setCreatedOnBeyonic(DateUtil.parseDate(paymentResponse.getCreated(), "YYYY-MM-DDTHH:MM:SSZ"));
        payment.setCurrency(paymentResponse.getCurrency());
        payment.setDescription(paymentResponse.getDescription());
        payment.setLastError(paymentResponse.getLastError());
        payment.setMetadata(new JSONObject(paymentResponse.getMetadata()).toString());
        payment.setModifiedOnBeyonic(DateUtil.parseDate(paymentResponse.getModified(), "YYYY-MM-DDTHH:MM:SSZ"));
        payment.setOrganization(paymentResponse.getOrganization());
        payment.setPaymentId(paymentResponse.getId());
        payment.setPaymentType(paymentResponse.getPaymentType());
        payment.setPhoneNumber(paymentResponse.getPhoneNos().get(0));
        payment.setRejectedBy(paymentResponse.getRejectedBy());
        payment.setRejectedReason(paymentResponse.getRejectedReason());
        payment.setRejectedTime(DateUtil.parseDate(paymentResponse.getRejectedTime(), "YYYY-MM-DDTHH:MM:SSZ"));
        payment.setState(BeyonicPaymentStateType.fromString(paymentResponse.getState()));
        payment.setUpdatedBy(paymentResponse.getUpdatedBy());
        payment.setIsPaymentPostedToMifos(false);

        paymentService.savePayment(payment);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
