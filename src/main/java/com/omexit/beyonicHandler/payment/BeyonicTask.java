package com.omexit.beyonicHandler.payment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.omexit.beyonicHandler.externalApi.beyonic.BeyonicPaymentStateType;
import com.omexit.beyonicHandler.externalApi.ExternalApiServiceGenerator;
import com.omexit.beyonicHandler.externalApi.beyonic.BeyonicAPIInterface;
import com.omexit.beyonicHandler.externalApi.paymentBridge.domain.AccessToken;
import com.omexit.beyonicHandler.jsonObjects.beyonicResponse.BeyonicPaymentResponse;
import com.omexit.beyonicHandler.jsonObjects.payment.PaymentRequest;
import com.omexit.beyonicHandler.util.DateUtil;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by aomeri on 9/22/16.
 * <p>
 * BeyonicTask spring managed bean with scope prototype, so that each request will return a new instance,
 * to run each individual thread.
 */
@Component
@Scope("prototype")
public class BeyonicTask implements Runnable {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ObjectMapper jacksonObjectMapper;
    private final PaymentService paymentService;
    private final ExternalApiServiceGenerator externalApiServiceGenerator;
    private String payload;

    @Value("${beyonicHandler.paymentCallbackUrl}")
    private String paymentCallbackUrl;
    @Value("${beyonicHandler.beyonic.baseUrl}")
    private String beyonicBaseUrl;
    @Value("${beyonicHandler.beyonic.token}")
    private String beyonicToken;


    @Autowired
    public BeyonicTask(ObjectMapper jacksonObjectMapper,
                       PaymentService paymentService,
                       ExternalApiServiceGenerator externalApiServiceGenerator) {
        this.jacksonObjectMapper = jacksonObjectMapper;
        this.paymentService = paymentService;
        this.externalApiServiceGenerator = externalApiServiceGenerator;
    }

    /**
     * Method to process request
     */
    @Override
    public void run() {
        try {
            //Create access token for Beyonic
            AccessToken accessToken = new AccessToken();
            accessToken.setTokenType("Token");
            accessToken.setAccessToken(beyonicToken);

            BeyonicAPIInterface beyonicAPIService =
                    externalApiServiceGenerator.createService(BeyonicAPIInterface.class, beyonicBaseUrl, accessToken);

            //Convert string to pojo
            PaymentRequest paymentRequest = jacksonObjectMapper.readValue(payload, PaymentRequest.class);
            logger.info("Request - {}", paymentRequest.toString());

            //Create new payment record
            Payment payment = new Payment();
            payment.setPaymentBridgeRef(paymentRequest.getId());
            payment.setPhoneNumber(paymentRequest.getPaymentAccount());
            payment.setCurrency(paymentRequest.getCurrency());
            payment.setAmount(Double.parseDouble(paymentRequest.getPaymentAccount()));
            payment.setDescription(paymentRequest.getDescription());
            payment.setIsPaymentPostedToMifos(false);
            payment.setPaymentStatus(0);

            payment = paymentService.savePayment(payment);


            String phoneNumber = paymentRequest.getPaymentAccount();
            String currency = paymentRequest.getCurrency();
            String amount = String.valueOf(paymentRequest.getTransactionAmount());
            String description = paymentRequest.getDescription();
            String paymentType = "money";

            logger.info("Beyonic.createPayment({},{},{},{},{},{})",
                    phoneNumber, currency, amount, description, paymentCallbackUrl, paymentType);

            //Send this request to beyonic and wait for response
            Call<BeyonicPaymentResponse> beyonicCall = beyonicAPIService.createPayment(phoneNumber,
                    currency, amount, description, paymentCallbackUrl, paymentType);
            Response<BeyonicPaymentResponse> beyonicResponse = beyonicCall.execute();

            logger.info(String.format("Beyonic.createPayment() - response {:isSuccess %s :statusCode %s, :message %s}",
                    beyonicResponse.isSuccessful(), beyonicResponse.code(), beyonicResponse.message()));

            if (beyonicResponse.isSuccessful()) {
                BeyonicPaymentResponse paymentResponse = beyonicResponse.body();

                if (paymentResponse != null) {
                    logger.info("Response Object: - {}", paymentResponse.toString());

                    payment.setAmount(Double.parseDouble(paymentResponse.getAmount()));
                    payment.setAuthor(paymentResponse.getAuthor());
                    payment.setCancelledBy(paymentResponse.getCancelledBy());
                    payment.setCancelledReason(paymentResponse.getCancelledReason());

                    if (paymentResponse.getCancelledTime() != null) {
                        payment.setCancelledTime(DateUtil.parseDate(paymentResponse.getCancelledTime(), DateUtil.BEYONIC_DATE_FORMAT));
                    }
                    if (paymentResponse.getCreated() != null) {
                        payment.setCreatedOnBeyonic(DateUtil.parseDate(paymentResponse.getCreated(), DateUtil.BEYONIC_DATE_FORMAT));
                    }
                    payment.setCurrency(paymentResponse.getCurrency());
                    payment.setDescription(paymentResponse.getDescription());
                    payment.setLastError(paymentResponse.getLastError());
                    payment.setMetadata(new JSONObject(paymentResponse.getMetadata()).toString());

                    if (paymentResponse.getModified() != null) {
                        payment.setModifiedOnBeyonic(DateUtil.parseDate(paymentResponse.getModified(), DateUtil.BEYONIC_DATE_FORMAT));
                    }
                    payment.setOrganization(paymentResponse.getOrganization());
                    payment.setPaymentId(paymentResponse.getId());
                    payment.setPaymentType(paymentResponse.getPaymentType());
                    payment.setPhoneNumber(paymentResponse.getPhoneNos().get(0));
                    payment.setRejectedBy(paymentResponse.getRejectedBy());
                    payment.setRejectedReason(paymentResponse.getRejectedReason());

                    if (paymentResponse.getRejectedTime() != null) {
                        payment.setRejectedTime(DateUtil.parseDate(paymentResponse.getRejectedTime(), DateUtil.BEYONIC_DATE_FORMAT));
                    }
                    payment.setState(BeyonicPaymentStateType.fromString(paymentResponse.getState()));
                    payment.setUpdatedBy(paymentResponse.getUpdatedBy());
                    payment.setIsPaymentPostedToMifos(false);
                    //Save payment and cross fingers for response
                    paymentService.savePayment(payment);
                } else {
                    logger.warn("Beyonic returned null object! Did not process the request fully!");
                }
            } else {
                logger.warn("Beyonic call was not successful.");
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * Method to set request payload
     *
     * @param payload - JSON String of the request
     */
    public void setPayload(String payload) {
        this.payload = payload;
    }
}
