package com.omexit.beyonicHandler.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Created by aomeri on 9/22/16.
 */
@Configuration
public class AppConfig {
    @Value("${beyonicHandler.paymentProcessorThreadPoolSize:5}")
    private int paymentProcessorThreadPoolSize;
    @Value("${beyonicHandler.paymentProcessorMaxThreadPoolSize:20}")
    private int paymentProcessorMaxThreadPoolSize;
    @Value("${beyonicHandler.paymentProcessorQueueCapacity:50}")
    private int paymentProcessorQueueCapacity;

    /**
     * Create thread pool bean configuration
     *
     * @return pool - ThreadPoolTaskExecutor object
     */
    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(paymentProcessorThreadPoolSize);
        pool.setMaxPoolSize(paymentProcessorMaxThreadPoolSize);
        pool.setWaitForTasksToCompleteOnShutdown(true);
        pool.setQueueCapacity(paymentProcessorQueueCapacity);
        pool.setThreadNamePrefix("PaymentProcessor-");
        return pool;
    }
}
