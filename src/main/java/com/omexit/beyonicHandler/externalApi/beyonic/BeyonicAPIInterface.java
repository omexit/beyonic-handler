package com.omexit.beyonicHandler.externalApi.beyonic;

import com.omexit.beyonicHandler.jsonObjects.beyonicResponse.BeyonicPaymentResponse;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * Created by Antony on 2/15/2016.
 */
public interface BeyonicAPIInterface {
	@FormUrlEncoded
    @POST("payments")
    Call<BeyonicPaymentResponse> createPayment(@Field("phonenumber") String phoneNumber,
                                                @Field("currency") String currency,
                                                @Field("amount") String amount,
                                                @Field("description") String description,
                                                @Field("callback_url") String callbackUrl,
                                                @Field("payment_type") String paymentType);
}
