package com.omexit.beyonicHandler.externalApi.paymentBridge;

import com.omexit.beyonicHandler.externalApi.paymentBridge.domain.AccessToken;
import com.omexit.beyonicHandler.externalApi.paymentBridge.domain.PaymentAck;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.Map;

/**
 * Created by aomeri on 9/24/16.
 */
public interface MifosBridgeApiInterface {
    @FormUrlEncoded
    @POST("oauth/token")
    Call<AccessToken> getAccessToken(@Field("client_id") String clientId,
                                     @Field("client_secret") String clientSecret,
                                     @Field("code") String code,
                                     @Field("grant_type") String grantType);

    Call<PaymentAck> getClient(@Path("paymentId") Long paymentId, @QueryMap Map<String, String> queryParams);
}
