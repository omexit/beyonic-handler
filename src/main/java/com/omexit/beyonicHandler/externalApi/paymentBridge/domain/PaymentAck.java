package com.omexit.beyonicHandler.externalApi.paymentBridge.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by Antony on 2/18/2016.
 */
@Data
public class PaymentAck {
    @JsonProperty("external_id")
    private String externalId;
    @JsonProperty("status")
    private String status;
    @JsonProperty("message")
    private String message;

}
